<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201124195855 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE request ADD number VARCHAR(30) NOT NULL');
        $this->addSql('ALTER TABLE stock RENAME INDEX fk_4b3656603d8ae468 TO IDX_4B365660E308AC6F');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE request DROP number');
        $this->addSql('ALTER TABLE stock RENAME INDEX idx_4b365660e308ac6f TO FK_4B3656603D8AE468');
    }
}
