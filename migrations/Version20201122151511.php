<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122151511 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE material (id INT AUTO_INCREMENT NOT NULL, article VARCHAR(30) NOT NULL, name LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE request (id INT AUTO_INCREMENT NOT NULL, request_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE request_item (id INT AUTO_INCREMENT NOT NULL, request_id INT NOT NULL, material_id INT NOT NULL, amount NUMERIC(10, 2) NOT NULL, supply_date DATE NOT NULL, INDEX IDX_60BC02E4427EB8A5 (request_id), INDEX IDX_60BC02E4E308AC6F (material_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock (id INT AUTO_INCREMENT NOT NULL, material_id INT NOT NULL, price NUMERIC(10, 2) NOT NULL, amount INT NOT NULL, supply_date DATE NOT NULL, involved_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE request_item ADD CONSTRAINT FK_60BC02E4427EB8A5 FOREIGN KEY (request_id) REFERENCES request (id)');
        $this->addSql('ALTER TABLE request_item ADD CONSTRAINT FK_60BC02E4E308AC6F FOREIGN KEY (material_id) REFERENCES material (id)');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656603D8AE468 FOREIGN KEY (material_id) REFERENCES material (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE request_item DROP FOREIGN KEY FK_60BC02E4E308AC6F');
        $this->addSql('ALTER TABLE request_item DROP FOREIGN KEY FK_60BC02E4427EB8A5');
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B3656603D8AE468');
        $this->addSql('DROP TABLE material');
        $this->addSql('DROP TABLE request');
        $this->addSql('DROP TABLE request_item');
        $this->addSql('DROP TABLE stock');
    }
}
