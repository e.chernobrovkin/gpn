<?php


namespace App\Service\Material;


use App\Entity\Material;
use Doctrine\ORM\EntityManagerInterface;

class MaterialService
{
	private EntityManagerInterface $em;

	public function __construct(EntityManagerInterface $entityManager) {
		$this->em = $entityManager;
	}

	public function create(string $article, string $name): Material {
		$material = Material::create($article, $name);

		$this->em->persist($material);
		$this->em->flush();

		return $material;
	}
}