<?php


namespace App\Service\Material\traits;


use App\Entity\Material;
use Doctrine\ORM\EntityManagerInterface;

trait FindOrInstanceMaterial
{
	/**
	 * Метод осуществляет поиск материала по арикулу и имени в базе данных.
	 * В случае ненахождения возвращает новый экземляр сущности материала
	 * с соответсвующими артикулом и названием
	 */
	private function findOrInstanceMaterialByArticleAndName(string $article, string $name, EntityManagerInterface $em) :Material
	{
		$materialRepository = $em->getRepository(Material::class);

		return $materialRepository->findOneByArticleAndName($article, $name) ?? new Material($article, $name);
	}
}