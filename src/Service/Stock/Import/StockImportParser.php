<?php


namespace App\Service\Stock\Import;


use App\Model\Stock\StockImportCollection;
use App\Model\Stock\StockImportModel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StockImportParser
{
	/*
	 * парсинг импортируемого файла эксель с запасами
	 */
	public function parseXLS(UploadedFile $file): StockImportCollection
	{
		$stocks = new StockImportCollection();

		$spreadSheet = IOFactory::load($file->getPathname());
		$sheet = $spreadSheet->getActiveSheet();

		if ($sheet->getHighestDataRow() < 2) {
			return $stocks;
		}

		$columnsMap = StockImportModel::$columnsMap;

		for ($i = 2; $i <= $sheet->getHighestDataRow(); $i++) {
			$materialArticle = $sheet->getCellByColumnAndRow($columnsMap['materialArticle'], $i)->getValue();
			$price = $sheet->getCellByColumnAndRow($columnsMap['price'], $i)->getValue();
			$amount = $sheet->getCellByColumnAndRow($columnsMap['amount'], $i)->getValue();
			$supplyDate = $sheet->getCellByColumnAndRow($columnsMap['supplyDate'], $i)->getValue();
			$materialName = $sheet->getCellByColumnAndRow($columnsMap['materialName'], $i)->getValue();
			$involveDate = $sheet->getCellByColumnAndRow($columnsMap['involveDate'], $i)->getValue();

			$stocks->append(new StockImportModel($materialArticle, $price, $amount, $supplyDate, $materialName, $involveDate));
		}

		return $stocks;
	}
}