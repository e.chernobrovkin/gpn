<?php


namespace App\Service\Stock\Import;


use App\Model\Stock\StockImportModel;

/*
 * Преобразует данные, полученные при импорте в данные для создания эксземпляра Stock
 */

class StockImportModelToEntityAdapter
{
	private StockImportModel $model;

	public function __construct(StockImportModel $stockImportModel)
	{
		$this->model = $stockImportModel;
	}

	public function getMaterialArticle(): string
	{
		return $this->model->getMaterialArticle();
	}

	public function getPrice(): float
	{
		return floatval(str_replace([',', ' '], ['.', ''], $this->model->getPrice()));
	}

	public function getAmount(): float
	{
		return intval(str_replace([',', ' '], ['.', ''], $this->model->getAmount()));
	}

	public function getSupplyDate(): \DateTime
	{
		return \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($this->model->getSupplyDate())));
	}

	public function getMaterialName(): string
	{
		return $this->model->getMaterialName();
	}

	public function getInvolveDate(): \DateTime
	{
		return \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($this->model->getInvolveDate())));
	}

}