<?php


namespace App\Service\Stock\Import;


use App\Entity\Material;
use App\Entity\Stock;
use App\Model\Stock\StockImportCollection;
use App\Repository\MaterialRepository;
use App\Service\Stock\StockService;
use PhpOffice\PhpSpreadsheet\IOFactory;

class StockImport
{
	private StockService $stockService;
	private MaterialRepository $materialRepository;

	public function __construct(StockService $stockService, MaterialRepository $materialRepository)
	{
		$this->stockService = $stockService;
		$this->materialRepository = $materialRepository;
	}

	/**
	 * @param StockImportCollection $stocks
	 * @return Stock[]
	 */
	public function importStocks(StockImportCollection $stocks): array
	{
		$insertedStocks = [];

		$materialNames = [];
		$materialByNamesByArticles = [];

		foreach ($stocks as $importStock) {
			$materialNames[$importStock->getMaterialName()] = $importStock->getMaterialName();
		}

		$materials = $this->materialRepository->findBy(['name' => $materialNames]);
		foreach ($materials as $material) {
			$materialByNamesByArticles[$material->getName()][$material->getArticle()] = $material;
		}

		foreach ($stocks as $importStock) {
			$adapter = new StockImportModelToEntityAdapter($importStock);

			$materialName = $adapter->getMaterialName();
			$materialArticle = $adapter->getMaterialArticle();
			$price = $adapter->getPrice();
			$amount = $adapter->getAmount();
			$supplyDate = $adapter->getSupplyDate();
			$involveDate = $adapter->getInvolveDate();

			if (!isset($materialByNamesByArticles[$materialName][$materialArticle])) {
				$materialByNamesByArticles[$materialName][$materialArticle] = Material::create($materialArticle, $materialName);
			}
			$material = $materialByNamesByArticles[$materialName][$materialArticle];

			$insertedStocks[] = $this->stockService->createStock($price, $amount, $supplyDate, $involveDate, $material);
		}

		return $insertedStocks;
	}
}