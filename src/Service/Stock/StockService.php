<?php


namespace App\Service\Stock;


use App\Service\Material\MaterialService;
use App\Dto\Stock\StockDetailsDto;
use App\Entity\Material;
use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;

class StockService
{
	private EntityManagerInterface $em;

	private MaterialService $materialService;

	public function __construct(EntityManagerInterface $entityManager, MaterialService $materialService)
	{
		$this->em = $entityManager;
		$this->materialService = $materialService;
	}

	public function createStock(string $price, int $amount, \DateTime $supplyDate, \DateTime $involvedDate, Material $material): Stock
	{
		$stock = Stock::create($price, $amount, $supplyDate, $involvedDate, $material);

		$this->save($stock);

		return $stock;
	}

	public function updateDetails(Stock $stock, StockDetailsDto $dto)
	{
		$stock->updateDetails($dto);

		$this->save($stock);
	}

	public function save(Stock $stock)
	{
		$this->em->persist($stock);
		$this->em->flush();
	}
}