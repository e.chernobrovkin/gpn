<?php


namespace App\Service\Request;


use App\Dto\Request\RequestDetailsDto;
use App\Entity\Request;
use Doctrine\ORM\EntityManagerInterface;

class RequestService
{
	private EntityManagerInterface $em;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;
	}

	public function create(\DateTime $requestDate, $number, $requestItems = []): Request
	{
		$request = Request::create($requestDate, $number);

		foreach ($requestItems as $requestItem) {
			$request->addRequestItem($requestItem);
		}

		$this->save($request);

		return $request;
	}

	public function updateDetails(Request $request, RequestDetailsDto $dto)
	{
		$request->updateDetails($dto);

		$this->save($request);
	}

	public function save(Request $request)
	{
		$this->em->persist($request);
		$this->em->flush();
	}
}