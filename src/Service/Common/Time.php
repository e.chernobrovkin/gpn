<?php


namespace App\Service\Common;


class Time
{
	public static function dateTimeIso8601($string): \DateTime
	{
		return \DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $string);
	}
}