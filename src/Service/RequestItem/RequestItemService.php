<?php


namespace App\Service\RequestItem;


use App\Dto\RequestItem\RequestItemDetailsDto;
use App\Entity\Material;
use App\Entity\Request;
use App\Entity\RequestItem;
use Doctrine\ORM\EntityManagerInterface;

class RequestItemService
{
	private EntityManagerInterface $em;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;
	}

	public function create(Request $request, Material $material, int $amount, \DateTime $supplyDate ): RequestItem {
		$requestItem = RequestItem::create($request, $material, $amount, $supplyDate);

		$this->em->persist($requestItem);
		$this->em->flush();

		return $requestItem;
	}

	public function updateDetails(RequestItem $requestItem, RequestItemDetailsDto $dto){
		$requestItem->updateDetails($dto);

		$this->em->persist($requestItem);
		$this->em->flush();
	}
}