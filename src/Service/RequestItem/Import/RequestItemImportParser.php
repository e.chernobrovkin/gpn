<?php


namespace App\Service\RequestItem\Import;


use App\Model\RequestItem\RequestItemImportCollection;
use App\Model\RequestItem\RequestItemImportModel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RequestItemImportParser
{
	/* парсинг файла эксель */
	public function parseXLS(UploadedFile $file): RequestItemImportCollection
	{
		$requestItems = new RequestItemImportCollection();

		$spreadSheet = IOFactory::load($file->getPathname());
		$sheet = $spreadSheet->getActiveSheet();

		if ($sheet->getHighestDataRow() < 2) {
			return $requestItems;
		}

		$columnsMap = RequestItemImportModel::$columnsMap;

		for ($i = 2; $i <= $sheet->getHighestDataRow(); $i++) {
			$requestNumber = $sheet->getCellByColumnAndRow($columnsMap['requestNumber'], $i)->getValue();
			$requestDate = $sheet->getCellByColumnAndRow($columnsMap['requestDate'], $i)->getValue();
			$materialArticle = $sheet->getCellByColumnAndRow($columnsMap['materialArticle'], $i)->getValue();
			$materialName = $sheet->getCellByColumnAndRow($columnsMap['materialName'], $i)->getValue();
			$amount = $sheet->getCellByColumnAndRow($columnsMap['amount'], $i)->getValue();
			$supplyDate = $sheet->getCellByColumnAndRow($columnsMap['supplyDate'], $i)->getValue();

			$requestItems->append(new RequestItemImportModel($requestNumber, $requestDate, $materialArticle, $materialName, $amount, $supplyDate));
		}

		return $requestItems;
	}
}