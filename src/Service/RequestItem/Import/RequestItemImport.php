<?php


namespace App\Service\RequestItem\Import;

use App\Service\Request\RequestService;
use App\Entity\Material;
use App\Entity\Request;
use App\Entity\RequestItem;
use App\Model\RequestItem\RequestItemImportCollection;
use App\Repository\MaterialRepository;
use App\Repository\RequestRepository;

class RequestItemImport
{
	private RequestService $requestService;
	private RequestRepository $requestRepository;
	private MaterialRepository $materialRepository;

	public function __construct(RequestService $requestSerivce, RequestRepository $requestRepository, MaterialRepository $materialRepository)
	{
		$this->requestService = $requestSerivce;
		$this->requestRepository = $requestRepository;
		$this->materialRepository = $materialRepository;
	}

	public function importRequestItems(RequestItemImportCollection $requestItems): array
	{
		$insertedRequestItems = [];
		$requestNumbers = [];
		$requestsByNumber = [];

		$materialNames = [];
		$materialByNamesByArticles = [];

		foreach ($requestItems as $requestItem) {
			$requestNumbers[$requestItem->getRequestNumber()] = $requestItem->getRequestNumber();

			$materialNames[$requestItem->getMaterialName()] = $requestItem->getMaterialName();
		}

		$requests = $this->requestRepository->findBy(['number' => $requestNumbers]);
		foreach ($requests as $request) {
			$requestsByNumber[$request->getNumber()] = $request;
		}

		$materials = $this->materialRepository->findBy(['name' => $materialNames]);
		foreach ($materials as $material) {
			$materialByNamesByArticles[$material->getName()][$material->getArticle()] = $material;
		}

		foreach ($requestItems as $requestItem) {
			$requestNumber = $requestItem->getRequestNumber();
			$materialName = $requestItem->getMaterialName();
			$materialArticle = $requestItem->getMaterialArticle();

			if (!isset($requestsByNumber[$requestNumber])) {
				$requestsByNumber[$requestNumber] = Request::create(\DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($requestItem->getRequestDate()))), $requestNumber);
			}
			$request = $requestsByNumber[$requestNumber];

			if (!isset($materialByNamesByArticles[$materialName][$materialArticle])) {
				$materialByNamesByArticles[$materialName][$materialArticle] = Material::create($materialArticle, $materialName);
			}
			$material = $materialByNamesByArticles[$materialName][$materialArticle];

			$requestItem = RequestItem::create($request, $material, floatval($requestItem->getAmount()), \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($requestItem->getSupplyDate()))));

			$requestsByNumber[$requestNumber]->addRequestItem($requestItem);
			$insertedRequestItems[] = $requestItem;
		}

		foreach ($requestsByNumber as $request) {
			$this->requestService->save($request);
		}
		return $insertedRequestItems;
	}
}