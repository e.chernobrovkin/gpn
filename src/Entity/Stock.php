<?php

namespace App\Entity;

use App\Dto\Stock\StockDetailsDto;
use App\Repository\StockRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Запас
 *
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 * @Groups({
	 *     "appropriateStock",
	 *     "requestItemGet",
	 *     "requestGet",
	 *     "requestListGet",
	 *     "stockGet"
	 * })
	 */
	private int $id;

	/**
	 * Рекомендованная цена реализации
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 * @Groups({
	 *     "stockGet",
	 *     "appropriateStock",
	 *     "stockCreate",
	 *     "StockUpdate",
	 * })
	 */
	private float $price;

	/**
	 * Количество
	 *
	 * @ORM\Column(type="integer")
	 * @Groups({
	 *     "stockGet",
	 *     "appropriateStock",
	 *     "requestItemGet",
	 *     "stockCreate",
	 *     "StockUpdate",
	 * })
	 */
	private int $amount;

	/**
	 * Дата поставки
	 *
	 * @ORM\Column(type="date")
	 * @Groups({
	 *     "stockGet",
	 *     "appropriateStock",
	 *     "requestItemGet",
	 *     "stockCreate",
	 *     "StockUpdate",
	 * })
	 */
	private \DateTime $supplyDate;

	/**
	 * Дата вовлечения
	 *
	 * @ORM\Column(type="date")
	 * @Groups({"stockGet",
	 *     "appropriateStock",
	 *     "requestItemGet",
	 *     "stockCreate",
	 *     "StockUpdate",
	 * })
	 */
	private \DateTime $involvedDate;

	/**
	 * Сущность материала запаса
	 *
	 * @ORM\ManyToOne(targetEntity=Material::class, cascade={"persist"})
	 * @ORM\JoinColumn(name="material_id", nullable=false)
	 * @Groups({
	 *     "stockGet",
	 *     "requestItemGet",
	 *     "appropriateStock",
	 *     "stockCreate",
	 * })
	 */
	private Material $material;

	public function __construct(string $price, int $amount, \DateTime $supplyDate, \DateTime $involvedDate, Material $material)
	{
		$this->price = $price;
		$this->amount = $amount;
		$this->supplyDate = $supplyDate;
		$this->involvedDate = $involvedDate;
		$this->material = $material;
	}

	public function getId(): ?int
	{
		return $this->id;
	}


	public function getPrice(): ?string
	{
		return $this->price;
	}


	public function getAmount(): ?int
	{
		return $this->amount;
	}


	public function getSupplyDate(): ?\DateTimeInterface
	{
		return $this->supplyDate;
	}


	public function getInvolvedDate(): ?\DateTimeInterface
	{
		return $this->involvedDate;
	}


	public function getMaterial(): ?Material
	{
		return $this->material;
	}

	public static function create(string $price, int $amount, \DateTime $supplyDate, \DateTime $involvedDate, Material $material): self
	{
		$entity = new self($price, $amount, $supplyDate, $involvedDate, $material);

		return $entity;
	}

	public function updateDetails(StockDetailsDto $dto)
	{
		$this->price = $dto->price;
		$this->amount = $dto->amount;
		$this->supplyDate = $dto->supplyDate;
		$this->involvedDate = $dto->involvedDate;
	}
}
