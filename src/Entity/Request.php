<?php

namespace App\Entity;

use App\Dto\Request\RequestDetailsDto;
use App\Repository\RequestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Заявка
 *
 * @ORM\Entity(repositoryClass=RequestRepository::class)
 */
class Request
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 * @Groups({"requestGet", "requestItemGet"})
	 */
	private int $id;
	/**
	 * Номер заявки
	 *
	 * @ORM\Column(type="string", length=30, unique=true)
	 * @Groups({
	 *     "requestGet",
	 *     "requestListGet",
	 *     "requestCreate",
	 *     "requestItemGet",
	 *     "requestItemCreate"
	 * })
	 */
	private string $number;
	/**
	 * Дата заявки
	 *
	 * @ORM\Column(type="date")
	 * @Groups({
	 *     "requestGet",
	 *     "requestListGet",
	 *     "requestCreate",
	 *     "requestItemGet",
	 *     "requestUpdate",
	 * })
	 */
	private \DateTime $requestDate;

	/**
	 * Запрашиваемые позиции
	 *
	 * @ORM\OneToMany(targetEntity=RequestItem::class, mappedBy="request", cascade="persist")
	 * @Groups({
	 *     "requestGet",
	 *     "requestListGet",
	 *     "requestCreate"
	 * })
	 */
	private iterable $requestItems;

	public function __construct(\DateTime $requestDate, string $number)
	{
		$this->requestDate = $requestDate;
		$this->number = $number;
		$this->requestItems = new ArrayCollection();
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getRequestDate(): \DateTime
	{
		return $this->requestDate;
	}

	public function getNumber(): string
	{
		return $this->number;
	}

	/**
	 * @return Collection|RequestItem[]
	 */
	public function getRequestItems(): Collection
	{
		return $this->requestItems;
	}

	public function addRequestItem(RequestItem $requestItem): self
	{
		if (!$this->requestItems->contains($requestItem)) {
			$this->requestItems[] = $requestItem;
			$requestItem->setRequest($this);
		}

		return $this;
	}

	public function removeRequestItem(RequestItem $requestItem): self
	{
		if ($this->requestItems->removeElement($requestItem)) {

			if ($requestItem->getRequest() === $this) {
				$requestItem->setRequest(null);
			}
		}

		return $this;
	}

	public function updateDetails(RequestDetailsDto $dto)
	{
		$this->requestDate = $dto->requestDate;
	}

	public static function create(\DateTime $requestDate, string $number)
	{
		$entity = new static($requestDate, $number);

		return $entity;
	}
}
