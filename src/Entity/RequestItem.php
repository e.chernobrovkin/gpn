<?php

namespace App\Entity;

use App\Dto\RequestItem\RequestItemDetailsDto;
use App\Repository\RequestItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Запрашиваемая позиция в заявке
 *
 * @ORM\Entity(repositoryClass=RequestItemRepository::class)
 */
class RequestItem
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 * @Groups({
	 *     "requestGet",
	 *     "requestListGet",
	 *     "requestItemGet"
	 * })
	 */
	private int $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private int $requestID;

	/**
	 * @ORM\Column(type="integer")
	 */
	private int $materialID;

	/**
	 * Количество запрашиваемых материалов
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 * @Groups({
	 *     "requestGet",
	 *     "requestListGet",
	 *     "requestItemGet",
	 *     "requestItemCreate",
	 *     "requestCreate",
	 *     "requestItemCreate",
	 *     "requestItemUpdate"
	 * })
	 */
	private float $amount;

	/**
	 * Первоначальная дата поставки
	 *
	 * @ORM\Column(type="date")
	 * @Groups({
	 *     "requestGet",
	 *     "requestListGet",
	 *     "requestItemGet",
	 *     "requestItemCreate",
	 *     "requestCreate",
	 *     "requestItemCreate",
	 *     "requestItemUpdate"
	 * })
	 */
	private \DateTime $supplyDate;

	/**
	 * Сущность заявки
	 *
	 * @ORM\ManyToOne(targetEntity=Request::class, inversedBy="requestItems", cascade={"remove"})
	 * @ORM\JoinColumn(name="request_id", nullable=false, onDelete="CASCADE")
	 * @Groups({"requestItemGet"})
	 */
	private ?Request $request;

	/**
	 * Сущность запрашиваемого материала
	 *
	 * @ORM\ManyToOne(targetEntity=Material::class, cascade="persist")
	 * @ORM\JoinColumn(name="material_id",nullable=false)
	 * @Groups({
	 *     "requestItemGet",
	 *     "requestGet",
	 *     "requestListGet",
	 *     "requestCreate",
	 *     "requestItemCreate",
	 *     })
	 */
	private Material $material;

	public function __construct(float $amount, \DateTime $supplyDate, Material $material)
	{
		$this->amount = $amount;
		$this->supplyDate = $supplyDate;
		$this->material = $material;
		$this->appropriateStocks = new ArrayCollection();
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getRequestID(): int
	{
		return $this->requestID;
	}

	public function getMaterialID(): int
	{
		return $this->materialID;
	}

	public function getAmount(): float
	{
		return $this->amount;
	}

	public function getSupplyDate(): \DateTime
	{
		return $this->supplyDate;
	}

	public function getRequest(): ?Request
	{
		return $this->request;
	}

	public function setRequest(?Request $request): self
	{
		$this->request = $request;

		return $this;
	}

	public function getMaterial(): Material
	{
		return $this->material;
	}

	public static function create(Request $request, Material $material, float $amount, \DateTime $supplyDate): self
	{
		$entity = new static($amount, $supplyDate, $material);
		$entity->request = $request;

		return $entity;
	}

	public function updateDetails(RequestItemDetailsDto $dto)
	{
		$this->amount = $dto->amount;
		$this->supplyDate = $dto->supplyDate;
	}
}
