<?php

namespace App\Entity;

use App\Repository\MaterialRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * материал
 *
 * @ORM\Entity(repositoryClass=MaterialRepository::class)
 */
class Material
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *     "requestItemGet",
     *     "requestGet",
     *     "requestListGet",
     *     "stockGet",
     *     "stockCreate",
     * })
     */
    private int $id;
    /**
     * Артикул
     *
     * @ORM\Column(type="string", length=30)
     * @Groups({
     *     "requestItemGet",
     *     "requestGet",
     *     "requestListGet",
     *     "stockGet",
     *     "stockCreate",
     *     "requestCreate",
     *     "requestItemCreate"})
     */
    private string $article;

    /**
     * Краткий текст материала
     *
     * @ORM\Column(type="text")
     * @Groups({
     *     "requestItemGet",
     *     "requestGet",
     *     "requestListGet",
     *     "stockGet",
     *     "stockCreate",
     *     "requestCreate",
     *     "requestItemCreate"
     * })
     */
    private string $name;

    public function __construct(string $article, string $name) {
    	$this->article = $article;
    	$this->name = $name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?string
    {
        return $this->article;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public static function create(string $article, string $name) {
	    $entity = new static($article, $name);

	    return $entity;
    }
}
