<?php


namespace App\Api;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait RequestTrait
{
	private function convertRequestToArray(Request $request): array
	{
		$data = json_decode($request->getContent(), true);
		if (JSON_ERROR_NONE !== json_last_error() || null === $data) {
			throw new BadRequestHttpException('Invalid JSON.');
		}

		return $data;
	}
}