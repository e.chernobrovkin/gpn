<?php


namespace App\Api\Stock\methods;

use App\Api\RequestTrait;
use App\Service\Common\Time;
use App\Service\Material\traits\FindOrInstanceMaterial;
use App\Service\Stock\StockService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * POST /stocks
 * Создание новой позиции запасов
 */
class StockCreate
{
	use RequestTrait;
	use FindOrInstanceMaterial;

	private StockService $stockService;
	private EntityManagerInterface $em;

	public function __construct(EntityManagerInterface $em, StockService $stockService)
	{
		$this->stockService = $stockService;
		$this->em = $em;
	}

	public function __invoke(Request $request)
	{
		$data = $this->convertRequestToArray($request);

		$name = $data['material']['name'];
		$article = $data['material']['article'];

		$material = $this->findOrInstanceMaterialByArticleAndName($article, $name, $this->em);

		return $this->stockService->createStock($data['price'], $data['amount'], Time::dateTimeIso8601($data['supplyDate']), Time::dateTimeIso8601($data['involvedDate']), $material);
	}
}