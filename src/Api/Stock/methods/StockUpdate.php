<?php


namespace App\Api\Stock\methods;


use App\Api\RequestTrait;
use App\Service\Common\Time;
use App\Service\Stock\StockService;
use App\Dto\Stock\StockDetailsDto;
use App\Entity\Stock;
use Symfony\Component\HttpFoundation\Request;

/**
 * PUT /stocks/{id}
 * Обновление позиции запасов
 */
class StockUpdate
{
	use RequestTrait;

	private StockService $stockService;

	public function __construct(StockService $stockService)
	{
		$this->stockService = $stockService;
	}

	public function __invoke(Stock $data, Request $request)
	{
		$requestData = $this->convertRequestToArray($request);

		$dto = new StockDetailsDto();

		$dto->price = $requestData['price'];
		$dto->amount = $requestData['amount'];
		$dto->supplyDate = Time::dateTimeIso8601($requestData['supplyDate']);
		$dto->involvedDate = Time::dateTimeIso8601($requestData['involvedDate']);

		$this->stockService->updateDetails($data, $dto);

		return $data;
	}
}