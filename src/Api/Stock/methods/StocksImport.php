<?php


namespace App\Api\Stock\methods;


use App\Service\Stock\Import\StockImport;
use App\Service\Stock\Import\StockImportParser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * POST /stocks/import
 * Импорт запасов
 */
class StocksImport
{
	private StockImportParser $parser;
	private StockImport $importService;

	public function __construct(StockImportParser $parser, StockImport $importService)
	{
		$this->parser = $parser;
		$this->importService = $importService;
	}

	public function __invoke(Request $request)
	{
		$uploadedFile = $request->files->get('file');

		if (!$uploadedFile) {
			throw new BadRequestHttpException('"file" is required');
		}

		$stocksRaw = $this->parser->parseXLS($uploadedFile);

		return $this->importService->importStocks($stocksRaw);
	}
}