<?php


namespace App\Api\RequestItem\Dto;


use App\Entity\Material;
use App\Entity\Request;
use App\Entity\RequestItem;
use App\Entity\Stock;
use DateTime;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * DTO позиции заявки
 */
class RequestItemStockedDto
{
	/**
	 * @Groups({"requestGet", "requestItemGet"})
	 */
	public int $id;
	/**
	 * @Groups({"requestGet", "requestItemGet", "requestItemCreate"})
	 */

	public float $amount;
	/**
	 * @Groups({"requestGet", "requestItemGet", "requestItemCreate"})
	 */
	public DateTime $supplyDate;
	/**
	 * @Groups({"requestItemGet"})
	 * @var Request
	 */
	public ?Request $request;
	/**
	 * @Groups({"requestGet","requestItemGet", "requestItemUpdate"})
	 * @var Material
	 */
	public Material $material;
	/**
	 * @Groups({"requestItemGet", "requestGet", "requestListGet"})
	 * @var Stock[]
	 */
	public iterable $appropriateStocks;

	public function __construct(RequestItem $requestItem, array $stocks = [])
	{
		$this->id = $requestItem->getId();
		$this->amount = $requestItem->getAmount();
		$this->supplyDate = $requestItem->getSupplyDate();
		$this->request = $requestItem->getRequest();
		$this->material = $requestItem->getMaterial();
		$this->appropriateStocks = $stocks;
	}
}