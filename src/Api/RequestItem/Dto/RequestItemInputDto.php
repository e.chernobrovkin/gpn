<?php


namespace App\Api\RequestItem\Dto;


use App\Api\Request\Dto\RequestOnlyNumberDto;
use App\Entity\Material;
use App\Entity\Request;
use App\Entity\RequestItem;
use DateTime;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiProperty;

class RequestItemInputDto
{
	/**
	 * @Groups({"requestItemCreate"})
	 * @ApiProperty(
	 *     attributes={
	 *         "openapi_context"={
	 *             "type"="string",
	 *         }
	 *     }
	 * )
	 */
	public float $amount;
	/**
	 * @Groups({"requestItemCreate"})
	 */
	public DateTime $supplyDate;
	/**
	 * @Groups({"requestItemCreate"})
	 */
	public RequestOnlyNumberDto $request;
	/**
	 * @Groups({"requestItemCreate"})
	 */
	public Material $material;

	public function __construct(RequestItem $requestItem)
	{
		$this->amount = $requestItem->getAmount();
		$this->supplyDate = $requestItem->getSupplyDate();
		$this->request = new RequestOnlyNumberDto($requestItem->getRequest());
		$this->material = $requestItem->getMaterial();
	}
}