<?php


namespace App\Api\RequestItem\methods;


use App\Api\RequestTrait;
use App\Service\Common\Time;
use App\Service\Material\traits\FindOrInstanceMaterial;
use App\Service\RequestItem\RequestItemService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * POST /requestitems
 * Создание новой позиции заявки
 */
class RequestItemCreate
{
	use RequestTrait;
	use FindOrInstanceMaterial;

	private RequestItemService $requestItemService;

	private EntityManagerInterface $em;

	public function __construct(EntityManagerInterface $em, RequestItemService $requestItemService)
	{
		$this->requestItemService = $requestItemService;
		$this->em = $em;
	}

	public function __invoke(Request $httpRequest)
	{
		/**
		 * @var array $requests
		 */

		$data = $this->convertRequestToArray($httpRequest);

		$requestRepository = $this->em->getRepository(\App\Entity\Request::class);
		$requests = $requestRepository->findBy(['number' => $data['request']['number']]);

		if (!count($requests)) {
			throw new HttpException(404, 'Not Found');
		}

		$material = $this->findOrInstanceMaterialByArticleAndName($data['material']['article'], $data['material']['name'], $this->em);

		return $this->requestItemService->create($requests[0], $material, $data['amount'], Time::dateTimeIso8601($data['supplyDate']));
	}
}