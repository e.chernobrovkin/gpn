<?php


namespace App\Api\RequestItem\methods;


use App\Service\RequestItem\Import\RequestItemImport;
use App\Service\RequestItem\Import\RequestItemImportParser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RequestItemsImport
{
	private RequestItemImport $importService;
	private RequestItemImportParser $parser;

	public function __construct(RequestItemImportParser $parser, RequestItemImport $requestItemImport)
	{
		$this->parser = $parser;
		$this->importService = $requestItemImport;
	}

	public function __invoke(Request $request)
	{
		$uploadedFile = $request->files->get('file');

		if (!$uploadedFile) {
			throw new BadRequestHttpException('"file" is required');
		}

		$requestItemsRaw = $this->parser->parseXLS($uploadedFile);

		return $this->importService->importRequestItems($requestItemsRaw);
	}
}