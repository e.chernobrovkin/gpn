<?php


namespace App\Api\RequestItem\methods;


use App\Api\RequestTrait;
use App\Service\Common\Time;
use App\Service\RequestItem\RequestItemService;
use App\Dto\RequestItem\RequestItemDetailsDto;
use Symfony\Component\HttpFoundation\Request;

/**
 * PUT /requestitems/{id}
 * Обновление позиции заявки
 */
class RequestItemUpdate
{
	use RequestTrait;

	private RequestItemService $requestItemService;

	public function __construct(RequestItemService $requestItemService)
	{
		$this->requestItemService = $requestItemService;
	}

	public function __invoke($data, Request $request)
	{
		$requestData = $this->convertRequestToArray($request);

		$dto = new RequestItemDetailsDto();
		$dto->amount = $requestData['amount'];
		$dto->supplyDate = Time::dateTimeIso8601($requestData['supplyDate']);

		$this->requestItemService->updateDetails($data, $dto);

		return $data;
	}
}