<?php


namespace App\Api\Request\Dto;

use App\Entity\Request;
use Symfony\Component\Serializer\Annotation\Groups;

class RequestOnlyNumberDto
{
	/**
	 * @Groups({"requestItemCreate"})
	 */
	public string $number;
	
	public function __construct(?Request $request) {
		$this->number = $request->getNumber();
	}
}