<?php


namespace App\Api\Request\Dto;


use App\Api\RequestItem\Dto\RequestItemStockedDto;
use App\Entity\Request;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

class RequestStockedDto
{
	/**
	 * @Groups({
	 *     "requestGet",
	 *     "requestItemGet",
	 * })
	 */
	public int $id;
	/**
	 * @Groups({
	 *     "requestGet",
	 *     "requestPost",
	 *     "requestItemGet",
	 * })
	 */
	public DateTime $requestDate;
	/**
	 * @Groups({
	 *     "requestGet",
	 *     "requestPost",
	 *     "requestItemGet",
	 *     "appropriateStock",
	 * })
	 * @var RequestItemStockedDto[]|ArrayCollection
	 */
	public iterable $requestItems = [];

	public function __construct(Request $request, iterable $requestItems)
	{
		$this->id = $request->getId();
		$this->requestDate = $request->getRequestDate();
		$this->requestItems = $requestItems;
	}
}