<?php


namespace App\Api\Request\methods;


use App\Api\RequestTrait;
use App\Service\Common\Time;
use App\Service\Request\RequestService;
use App\Dto\Request\RequestDetailsDto;
use Symfony\Component\HttpFoundation\Request;

/**
 * PUT /requests/{id}
 * Обновление заявки
 */
class RequestUpdate
{
	use RequestTrait;

	private RequestService $requestService;

	public function __construct(RequestService $requestService)
	{
		$this->requestService = $requestService;
	}

	public function __invoke(\App\Entity\Request $data, Request $request)
	{
		$requestData = $this->convertRequestToArray($request);

		$dto = new RequestDetailsDto();
		$dto->requestDate = Time::dateTimeIso8601($requestData['requestDate']);

		$this->requestService->updateDetails($data, $dto);

		return $data;
	}
}