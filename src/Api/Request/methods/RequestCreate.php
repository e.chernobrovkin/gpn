<?php
/**
 * Duotek.ru
 * User: evgeniy
 * Date: 24.11.20
 * Time: 19:15
 */

namespace App\Api\Request\methods;


use App\Api\RequestTrait;
use App\Service\Common\Time;
use App\Service\Material\traits\FindOrInstanceMaterial;
use App\Service\Request\RequestService;
use App\Entity\RequestItem;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * POST /requests
 * Создание новой заявки
 */
class RequestCreate
{
	use RequestTrait;
	use FindOrInstanceMaterial;

	private RequestService $requestService;
	private EntityManagerInterface $em;

	public function __construct(EntityManagerInterface $em, RequestService $requestService)
	{
		$this->requestService = $requestService;
		$this->em = $em;
	}

	public function __invoke(Request $httpRequest)
	{

		$data = $this->convertRequestToArray($httpRequest);

		$requestItems = new ArrayCollection();

		$materials = [];
		foreach ($data['requestItems'] as $requestItem) {
			$article = $requestItem['material']['article'];
			$name = $requestItem['material']['name'];

			$materials = $this->addMaterial($name, $article, $materials);

			$requestItems->add(new RequestItem($requestItem['amount'], Time::dateTimeIso8601($requestItem['supplyDate']), $materials[$name][$article]));
		}

		return $this->requestService->create(Time::dateTimeIso8601($data['requestDate']), $data['number'], $requestItems);
	}

	private function addMaterial($name, $article, $materials)
	{
		if (isset($materials[$name][$article])) {
			return $materials;
		}

		$materials[$name][$article] = $this->findOrInstanceMaterialByArticleAndName($article, $name, $this->em);

		return $materials;
	}
}