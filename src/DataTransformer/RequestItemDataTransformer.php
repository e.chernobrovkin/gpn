<?php


namespace App\DataTransformer;


use App\Api\Request\Dto\RequestOnlyNumberDto;
use App\Api\RequestItem\Dto\RequestItemInputDto;
use App\Api\RequestItem\Dto\RequestItemStockedDto;
use App\Entity\RequestItem;
use App\Repository\StockRepository;

class RequestItemDataTransformer implements \ApiPlatform\Core\DataTransformer\DataTransformerInterface
{
	private StockRepository $stockRepository;

	public function __construct(StockRepository $stockRepository)
	{
		$this->stockRepository = $stockRepository;
	}

	/**
	 * @inheritDoc
	 * @var RequestItem $object
	 */
	public function transform($object, string $to, array $context = [])
	{
		if ($to === RequestItemStockedDto::class) {
			$stocks = $this->stockRepository->findAppropriateStocks($object);

			$requestItem = new RequestItemStockedDto($object, $stocks);

			return $requestItem;
		}

		if ($to === RequestItemInputDto::class) {
			$requestItem = new RequestItemInputDto($object);

			return $requestItem;
		}
	}

	/**
	 * @inheritDoc
	 */
	public function supportsTransformation($data, string $to, array $context = []): bool
	{
		return
			(RequestItemStockedDto::class === $to && $data instanceof RequestItem)
			|| (RequestItemInputDto::class === $to && $data instanceof RequestItem);
	}
}