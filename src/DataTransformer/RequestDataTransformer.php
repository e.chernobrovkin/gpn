<?php


namespace App\DataTransformer;


use App\Api\Request\Dto\RequestStockedDto;
use App\Api\RequestItem\Dto\RequestItemStockedDto;
use App\Entity\Request;
use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;

class RequestDataTransformer implements \ApiPlatform\Core\DataTransformer\DataTransformerInterface
{
	private StockRepository $stockRepository;

	public function __construct(StockRepository $stockRepository){
		$this->stockRepository = $stockRepository;
	}
	/**
	 * @inheritDoc
	 * @var Request $object
	 */
	public function transform($object, string $to, array $context = [])
	{
		$requestItems = new ArrayCollection();
		foreach ($object->getRequestItems() as $requestItem){
			$stocks = $this->stockRepository->findAppropriateStocks($requestItem);

			$requestItems->add(new RequestItemStockedDto($requestItem, $stocks));
		}

		return new RequestStockedDto($object, $requestItems);
	}

	/**
	 * @inheritDoc
	 */
	public function supportsTransformation($data, string $to, array $context = []): bool
	{
		return RequestStockedDto::class === $to && $data instanceof Request;
	}
}