<?php


namespace App\Model\Stock;


class StockImportCollection extends \ArrayIterator
{
	public function __construct(StockImportModel ...$items)
	{

		parent::__construct($items);
	}

	public function current(): StockImportModel
	{
		return parent::current();
	}

	public function offsetGet($offset): StockImportModel
	{
		return parent::offsetGet($offset);
	}

	public function append($value)
	{
		if ($value instanceof StockImportModel) {
			parent::append($value);
		} else {
			throw new \Exception('Appended must be a StockImportModel');
		}
	}

	public function offsetSet($index, $newval)
	{
		if ($newval instanceof StockImportModel) {
			parent::offsetSet($index, $newval);
		} else {
			throw new \Exception('Add must be a StockImportModel');
		}
	}
}