<?php


namespace App\Model\Stock;


class StockImportModel
{
	/*
	 * маппинг поля к номеру колонки в файле импорта
	 */
	public static $columnsMap = [
		'materialArticle' => 1,
		'price' => 2,
		'amount' => 3,
		'supplyDate' => 4,
		'materialName' => 5,
		'involveDate' => 6,
	];

	private string $materialArticle;

	private string $price;

	private string $amount;

	private string $supplyDate;

	private string $materialName;

	private string $involveDate;

	public function __construct(string $materialArticle, string $price, string $amount, string $supplyDate, string $materialName, string $involveDate)
	{
		$this->materialArticle = $materialArticle;
		$this->price = $price;
		$this->amount = $amount;
		$this->supplyDate = $supplyDate;
		$this->materialName = $materialName;
		$this->involveDate = $involveDate;
	}

	/**
	 * @return string
	 */
	public function getMaterialArticle(): string
	{
		return $this->materialArticle;
	}

	/**
	 * @return string
	 */
	public function getPrice(): string
	{
		return $this->price;
	}

	/**
	 * @return string
	 */
	public function getAmount(): string
	{
		return $this->amount;
	}

	/**
	 * @return string
	 */
	public function getSupplyDate(): string
	{
		return $this->supplyDate;
	}

	/**
	 * @return string
	 */
	public function getMaterialName(): string
	{
		return $this->materialName;
	}

	/**
	 * @return string
	 */
	public function getInvolveDate(): string
	{
		return $this->involveDate;
	}


}