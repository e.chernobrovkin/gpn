<?php


namespace App\Model\RequestItem;


class RequestItemImportModel
{
	/*
	 * маппинг поля к номеру колонки в файле импорта
	 */
	public static array $columnsMap = [
		'requestNumber' => 1,
		'requestDate' => 2,
		'materialArticle' => 3,
		'materialName' => 4,
		'amount' => 5,
		'supplyDate' => 6,
	];
	/*
	 * Номер заявки
	 */
	private string $requestNumber;
	/*
	 * Дата заявки
	 */
	private string $requestDate;
	/*
	 * Артикул запрашиваемого материала
	 */
	private string $materialArticle;
	/*
	 * Название запрашиваемого материала
	 */
	private string $materialName;
	/*
	 * Количество
	 */
	private float $amount;
	/*
	 * Дата поставки
	 */
	private string $supplyDate;

	public function __construct(string $requestNumber, string $requestDate,string $materialArticle, string $materialName, string $amount, string $supplyDate) {
		$this->requestNumber = $requestNumber;
		$this->requestDate = $requestDate;
		$this->materialArticle = $materialArticle;
		$this->materialName = $materialName;
		$this->amount = $amount;
		$this->supplyDate = $supplyDate;
	}

	/**
	 * @return string
	 */
	public function getRequestNumber(): string
	{
		return $this->requestNumber;
	}

	/**
	 * @return string
	 */
	public function getRequestDate(): string
	{
		return $this->requestDate;
	}

	/**
	 * @return string
	 */
	public function getMaterialName(): string
	{
		return $this->materialName;
	}

	/**
	 * @return string
	 */
	public function getMaterialArticle(): string
	{
		return $this->materialArticle;
	}

	/**
	 * @return string
	 */
	public function getAmount(): string
	{
		return $this->amount;
	}

	/**
	 * @return string
	 */
	public function getSupplyDate(): string
	{
		return $this->supplyDate;
	}


}