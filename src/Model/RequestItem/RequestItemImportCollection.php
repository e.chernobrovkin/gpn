<?php


namespace App\Model\RequestItem;

class RequestItemImportCollection extends \ArrayIterator
{
	public function __construct(RequestItemImportModel ...$items)
	{

		parent::__construct($items);
	}

	public function current(): RequestItemImportModel
	{
		return parent::current();
	}

	public function offsetGet($offset): RequestItemImportModel
	{
		return parent::offsetGet($offset);
	}

	public function append($value)
	{
		if ($value instanceof RequestItemImportModel) {
			parent::append($value);
		} else {
			throw new \Exception('Appended must be a RequestItemImportModel');
		}
	}

	public function offsetSet($index, $newval)
	{
		if ($newval instanceof RequestItemImportModel) {
			parent::offsetSet($index, $newval);
		} else {
			throw new \Exception('Add must be a  RequestItemImportModel');
		}
	}
}