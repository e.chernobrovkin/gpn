<?php


namespace App\Dto\RequestItem;


use DateTime;

class RequestItemDetailsDto
{
	/**
	 * @var int
	 */
	public int $amount;
	/**
	 * @var \DateTime
	 */
	public DateTime $supplyDate;
}