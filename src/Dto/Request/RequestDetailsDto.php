<?php


namespace App\Dto\Request;

use DateTime;

class RequestDetailsDto
{
	public DateTime $requestDate;
}