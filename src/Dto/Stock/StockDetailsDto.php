<?php


namespace App\Dto\Stock;


use DateTime;

class StockDetailsDto
{
	public float $price;

	public int $amount;
	
	public DateTime $supplyDate;

	public DateTime $involvedDate;
}