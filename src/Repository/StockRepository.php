<?php

namespace App\Repository;

use App\Entity\RequestItem;
use App\Entity\Stock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stock[]    findAll()
 * @method Stock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stock::class);
    }
	
	/**
	 * Поиск запасов для сопоставления с запрашиваемой потербностью
	 *
	 * @return Stock[]
	 */
    public function findAppropriateStocks(RequestItem $requestItem, int $minAmount = 1, int $offset=0, int $limit = 10): array
	{
		$name = $requestItem->getMaterial()->getName();
		
		$nameParts = explode(' ', $name);
		
		$rawQuery = 'SELECT s FROM App\Entity\Material m
				INNER JOIN App\Entity\Stock s WITH s.material = m.id AND s.amount >= :minAmount
				WHERE m.name LIKE :name'.implode(' OR m.name LIKE :name', array_keys($nameParts)).'
				GROUP BY s.id';
		
		$query = $this->getEntityManager()->createQuery($rawQuery)
			->setParameter('minAmount', $minAmount);
		
		foreach ($nameParts as $k=>$namePart) {
			$query->setParameter('name'.$k, '%'.$namePart.'%');
		}
		
		$query->setFirstResult($offset)
			->setMaxResults($limit);
		
		return $query->getResult();
	}
}
