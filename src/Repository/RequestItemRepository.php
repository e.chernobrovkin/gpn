<?php

namespace App\Repository;

use App\Entity\RequestItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RequestItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequestItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequestItem[]    findAll()
 * @method RequestItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequestItem::class);
    }
}
