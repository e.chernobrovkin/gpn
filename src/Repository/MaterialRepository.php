<?php

namespace App\Repository;

use App\Entity\Material;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Material|null find($id, $lockMode = null, $lockVersion = null)
 * @method Material|null findOneBy(array $criteria, array $orderBy = null)
 * @method Material[]    findAll()
 * @method Material[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, Material::class);
	}

	/**
	 * Поиск материала по артикулу и названию
	 */
	public function findOneByArticleAndName($article, $name) :?Material
	{
		return $this->createQueryBuilder('m')
		            ->andWhere('m.article = :article')
		            ->andWhere('m.name = :name')
		            ->setParameter('article', $article)
		            ->setParameter('name', $name)
		            ->getQuery()
		            ->getOneOrNullResult();
	}
}
